'use strict';

const expect = require('chai').expect;
const request = require('request');
const httpStatus = require('http-status');
const testData = require('../testData');

describe('userController', () => {
  let testHost;

  before(() => {
    testHost = `${process.env.TEST_HOST}/user`;
  });

  describe('GET user', () => {
    it('should return array of user models with valid params', (done) => {
      request({
        qs: {
          lat: testData.baseCoordinates.latitude,
          long: testData.baseCoordinates.longitude,
          maxDistance: testData.maxDistance,
          units: testData.units
        },
        url: testHost,
        json: true
      }, (err, res) => {
        _validateSuccessResponse(err, res);
        done();
      });
    });

    it('should return array of user models without params', (done) => {
      request({
        url: testHost,
        json: true
      }, (err, res) => {
        _validateSuccessResponse(err, res);
        done();
      });
    });

    it('should return bad request for invalid lat', (done) => {
      request({
        qs: {
          lat: 'invalidLat'
        },
        url: testHost,
        json: true
      }, (err, res) => {
        _validateBadRequest(err, res);
        done();
      });
    });

    it('should return bad request for invalid long', (done) => {
      request({
        qs: {
          long: 'invalidLong'
        },
        url: testHost,
        json: true
      }, (err, res) => {
        _validateBadRequest(err, res);
        done();
      });
    });

    it('should return bad request for invalid maxDistance', (done) => {
      request({
        qs: {
          maxDistance: 'invalidDistance'
        },
        url: testHost,
        json: true
      }, (err, res) => {
        _validateBadRequest(err, res);
        done();
      });
    });

    it('should return bad request for invalid units', (done) => {
      request({
        qs: {
          units: 'noUnit'
        },
        url: testHost,
        json: true
      }, (err, res) => {
        _validateBadRequest(err, res);
        done();
      });
    });
  });

  function _validateBadRequest(err, res) {
    expect(err).to.be.equal(null);
    expect(res.statusCode).to.be.equal(httpStatus.BAD_REQUEST);
  }

  function _validateSuccessResponse(err, res) {
    expect(err).to.be.equal(null);
    expect(res.statusCode).to.be.equal(httpStatus.OK);
    expect(res.body).to.be.an('array');
    expect(res.body).not.to.be.empty;
    const userModel = res.body[0];
    expect(userModel).to.have.all.keys('name', 'userId');
  }
});
