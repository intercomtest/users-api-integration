'use strict';

const expect = require('chai').expect;
const request = require('request');
const httpStatus = require('http-status');

describe('health check endpoint', () => {
  let address;

  before(() => {
    address = process.env.TEST_HOST;
  });

  it('returns status ok when the healthcheck is called', (done) => {
    request(`${address}/healthcheck`, (err, res) => {
      expect(err).to.be.equal(null);
      expect(res.statusCode).to.be.equal(httpStatus.OK);
      done();
    });
  });
});
