'use strict';

module.exports = function (grunt) {
  /* eslint-disable global-require, import/no-extraneous-dependencies */
  require('load-grunt-tasks')(grunt);

  const lintFiles = [
    'test/**/*.js',
    'gruntfile.js'
  ];

  grunt.initConfig({
    eslint: {
      validate: {
        options: {
          fix: false
        },
        src: lintFiles
      },
      fix: {
        options: {
          fix: true
        },
        src: lintFiles
      }
    }
  });
};
