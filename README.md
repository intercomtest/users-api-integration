# README #

Integration tests for the users-api project.

### Overview ###

The tests are dockerized and validate the API response models and input param validation.

The tests can be executed manually but are also executed within the Jenkins CI during deployment.
